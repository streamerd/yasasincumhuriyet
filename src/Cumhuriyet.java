import java.util.Date;

// Cumhuriyet : The Republic.
public class Cumhuriyet {

    private String name;
    private Date dateFound;
    private Integer population; // optional for constructor. Population might be unknown.
    private String capital; 
    private String oldCapital; // optional for constructor. (Capital city may not change in another fundamental historical change.)
    private String founderName;

    public Cumhuriyet(String name, Date dateFound, String capital, String founderName) {
        this.name = name;
        this.dateFound= dateFound;
        this.capital = capital;
        this.founderName = founderName;
    }

    public Cumhuriyet(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateFound() {
        return dateFound;
    }

    public void setDateFound(Date dateFound) {
        this.dateFound = dateFound;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getOldCapital() {
        return oldCapital;
    }

    public void setOldCapital(String oldCapital) {
        this.oldCapital = oldCapital;
    }

    public String getFounderName() {
        return founderName;
    }

    public void setFounderName(String founderName) {
        this.founderName = founderName;
    }

    @Override
    public String toString() {
        return "Cumhuriyet{" +
                "name='" + name + '\'' +
                ", dateFound=" + dateFound +
                ", population=" + population +
                ", capital='" + capital + '\'' +
                ", oldCapital='" + oldCapital + '\'' +
                ", founderName='" + founderName + '\'' +
                '}';
    }
}
